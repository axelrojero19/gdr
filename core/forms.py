from django import forms
from django.forms.formsets import formset_factory
from django.forms.widgets import DateInput
from .models import *
from tempus_dominus.widgets import DatePicker

class GrantGoalForm(forms.ModelForm):
    class Meta:
        model = GrantGoal
        fields = "__all__"
        exclude = ["timestamp", "final_date"]
        widgets = {
            "user": forms.Select(attrs={"type":"select","class":"form-select"}),
            "gg_title": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el nombre del GrantGoal"}),
            "description": forms.Textarea(attrs={"type":"text", "class":"form-control", "rows":3, "placeholder":"Escribe la descripcion del GrantGoal"}),
            "days_duration": forms.NumberInput(attrs={"type":"number", "class":"form-control"}),
            "status": forms.CheckboxInput(attrs={"type":"checkbox", "class":"form-checkbox"}),
            "state": forms.Select(attrs={"type":"select","class":"form-select"}),
            "sprint":forms.NumberInput(attrs={"type":"number", "class":"form-control"}),
            "slug":forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el codigo del GrantGoal"}),
        }


class UpdateGrantGoalForm(forms.ModelForm):
    class Meta:
        model = GrantGoal
        fields = "__all__"
        exclude = ["timestamp", "final_date"]
        widgets = {
            "user": forms.Select(attrs={"type":"select","class":"form-select"}),
            "gg_title": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el nombre del GrantGoal"}),
            "description": forms.Textarea(attrs={"type":"text", "class":"form-control", "rows":3, "placeholder":"Escribe la descripcion del GrantGoal"}),
            "days_duration": forms.NumberInput(attrs={"type":"number", "class":"form-control"}),
            "status": forms.CheckboxInput(attrs={"type":"checkbox", "class":"form-checkbox"}),
            "state": forms.Select(attrs={"type":"select","class":"form-select"}),
            "sprint":forms.NumberInput(attrs={"type":"number", "class":"form-control"}),
            "slug":forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el codigo del GrantGoal"}),
        }


class UpdateEmpleado(forms.ModelForm):
    class Meta:
        model = Empleados
        fields = "__all__"
        widgets = {
            'noempleado': forms.NumberInput(attrs={"type":"number", "class":"form-control", "placeholder":"Numero del empleado.", "min":"1"}),
            'nombrepila': forms.TextInput(attrs={"type":"text",'class': 'form-control', "placeholder":"Nombre."}),
            'primerapellido': forms.TextInput(attrs={"type":"text",'class': 'form-control', "placeholder":"Apelido paterno."}),
            'segundoapellido': forms.TextInput(attrs={"type":"text",'class': 'form-control', "placeholder":"Apellido materno."}),
            'calle': forms.TextInput(attrs={"type":"text",'class': 'form-control', "placeholder":"Calle."}),
            'numext': forms.TextInput(attrs={"type":"number", 'class': 'form-control', "placeholder":"Numero exterior."}),
            'colonia': forms.TextInput(attrs={"type":"text",'class': 'form-control', "placeholder":"Colonia."}),
            'numtel': forms.TextInput(attrs={"type":"text",'class': 'form-control', "placeholder":"Numero de telefono."}),
        }


class CrearEmpleado(forms.ModelForm):
    class Meta:
        model = Empleados
        fields = "__all__"
        widgets = {
            'noempleado': forms.NumberInput(attrs={"type":"number", "class":"form-control", "placeholder":"Numero del empleado.", "min":"1"}),
            'nombrepila': forms.TextInput(attrs={"type":"text",'class': 'form-control', "placeholder":"Nombre."}),
            'primerapellido': forms.TextInput(attrs={"type":"text",'class': 'form-control', "placeholder":"Apelido paterno."}),
            'segundoapellido': forms.TextInput(attrs={"type":"text",'class': 'form-control', "placeholder":"Apellido materno."}),
            'calle': forms.TextInput(attrs={"type":"text",'class': 'form-control', "placeholder":"Calle."}),
            'numext': forms.TextInput(attrs={"type":"number", 'class': 'form-control', "placeholder":"Numero exterior."}),
            'colonia': forms.TextInput(attrs={"type":"text",'class': 'form-control', "placeholder":"Colonia."}),
            'numtel': forms.TextInput(attrs={"type":"text",'class': 'form-control', "placeholder":"Numero de telefono."}),
        }


class CrearParadas(forms.ModelForm):
    class Meta:
        model = Paradas
        fields = "__all__"
        widgets = {
            "nombre": forms.Textarea(attrs={"type":"text", "class":"form-control", "rows":4, "placeholder":"Nombre de la parada."}),
            "clave": forms.NumberInput(attrs={"type":"number", "class":"form-control", "placeholder":"Código de la ruta.", "min":"1"}),
            "descripcion": forms.Textarea(attrs={"type":"text", "class":"form-control", "rows":4, "placeholder":"Descripción de la parada."}),
        }
        

class CrearRuta(forms.ModelForm):
    class Meta:
        model = Rutas
        fields = "__all__"
        exclude = []
        widgets = {
            "numeroconsecutivo": forms.NumberInput(attrs={"type":"number", "class":"form-control", "placeholder":"Numero de identificacion.", "min":"1"}),
            "noruta": forms.NumberInput(attrs={"type":"number", "class":"form-control", "placeholder":"Numero de ruta.", "min":"1"}),
            "nombre": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Nombre."}),
            "conductor": forms.Select(attrs={"type":"text", "class":"form-select"}),
            "transporte": forms.Select(attrs={"type":"text", "class":"form-select"}),
            "parada": forms.Select(attrs={"type":"text", "class":"form-select"}),
            "hora": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Hora."}),
            "fechainicio": DateInput(attrs={'type': 'date', 'class': 'form-control', 'placeholder': 'Fecha de inicio'}),
            "fechafinal": DateInput(attrs={'type': 'date', 'class': 'form-control', 'placeholder': 'Fecha final'}),
        }

    paradas = formset_factory(CrearParadas, extra = 1, can_delete=True)


class AddStop(forms.ModelForm):
    class Meta:
        model = Rutas
        fields = ['numeroconsecutivo','parada', 'hora', 'fechainicio', 'fechafinal']
        widgets = {
            "numeroconsecutivo": forms.NumberInput(attrs={"type":"number", "class":"form-control", "placeholder":"Numero de identificacion.", "min":"1"}),
            "noruta": forms.NumberInput(attrs={"type":"text", "class":"form-control", "placeholder":"Numero de ruta.", "min":"1"}),
            "nombre": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Nombre."}),
            "conductor": forms.Select(attrs={"type":"text", "class":"form-select"}),
            "transporte": forms.Select(attrs={"type":"text", "class":"form-select"}),
            "parada": forms.Select(attrs={"type":"text", "class":"form-select"}),
            "hora": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Hora."}),
            "fechainicio": DateInput(attrs={'type': 'date', 'class': 'form-control', 'placeholder': 'Fecha de inicio'}),
            "fechafinal": DateInput(attrs={'type': 'date', 'class': 'form-control', 'placeholder': 'Fecha final'}),
        }

    paradas = formset_factory(CrearParadas, extra = 1, can_delete=True)


class EliminarRuta(forms.ModelForm):
    class Meta:
        model = Rutas
        fields = "__all__"
        exclude = ["conductor", "transporte", "hora", "fechainicio", "fechafinal"]
        widgets = {
            "user": forms.Select(attrs={"type":"select", "class":"form-select"}),
            "parada": forms.Textarea(attrs={"type":"text", "class":"form-control", "rows":4, "placeholder":"Código de parada."}),
        }


class UpdateConductor(forms.ModelForm):
    administrador = forms.ModelChoiceField(queryset=Administrador.objects.all(), widget=forms.Select(attrs={'class': 'form-select'}))
    class Meta:
        model = Conductores
        fields = "__all__"
        widgets = {
            'noempleado': forms.NumberInput(attrs={"type":"number", "class":"form-control", "placeholder":"Numero del conductor.", "min":"1"}),
            'nombrepila': forms.TextInput(attrs={"type":"text",'class': 'form-control', "placeholder":"Nombre de pila."}),
            'primerapellido': forms.TextInput(attrs={"type":"text",'class': 'form-control', "placeholder":"Apelido paterno."}),
            'segundoapellido': forms.TextInput(attrs={"type":"text",'class': 'form-control', "placeholder":"Apellido materno."}),
            'numtel': forms.TextInput(attrs={"type":"text",'class': 'form-control', "placeholder":"Numero de telefono."}),
        }


class CreateConductor(forms.ModelForm):
    administrador = forms.ModelChoiceField(queryset=Administrador.objects.all(), widget=forms.Select(attrs={'class': 'form-select'}))
    class Meta:
        model = Conductores
        fields = "__all__"
        widgets = {
            'noempleado': forms.NumberInput(attrs={"type":"number", "class":"form-control", "placeholder":"Numero del conductor.", "min":"1"}),
            'nombrepila': forms.TextInput(attrs={"type":"text",'class': 'form-control', "placeholder":"Nombre de pila."}),
            'primerapellido': forms.TextInput(attrs={"type":"text",'class': 'form-control', "placeholder":"Apelido paterno."}),
            'segundoapellido': forms.TextInput(attrs={"type":"text",'class': 'form-control', "placeholder":"Apellido materno."}),
            'numtel': forms.TextInput(attrs={"type":"text",'class': 'form-control', "placeholder":"Numero de telefono."}),
              
        }


class UpdateAdministrador(forms.ModelForm):
    class Meta:
        model = Administrador
        fields = "__all__"
        widgets = {
            'noempleado': forms.NumberInput(attrs={"type":"number", "class":"form-control", "placeholder":"Numero del empleado.", "min":"1"}),
            'nombrepila': forms.TextInput(attrs={"type":"text",'class': 'form-control', "placeholder":"Nombre."}),
            'primerapellido': forms.TextInput(attrs={"type":"text",'class': 'form-control', "placeholder":"Apelido paterno."}),
            'segundoapellido': forms.TextInput(attrs={"type":"text",'class': 'form-control', "placeholder":"Apellido materno."}),
            'calle': forms.TextInput(attrs={"type":"text",'class': 'form-control', "placeholder":"Calle."}),
            'numext': forms.TextInput(attrs={"type":"number", 'class': 'form-control', "placeholder":"Numero exterior."}),
            'colonia': forms.TextInput(attrs={"type":"text",'class': 'form-control', "placeholder":"Colonia."}),
            'numtel': forms.TextInput(attrs={"type":"text",'class': 'form-control', "placeholder":"Numero de telefono."}),
        }


class CreateAdministrador(forms.ModelForm):
    class Meta:
        model = Administrador
        fields = "__all__"
        widgets = {
            'noempleado': forms.NumberInput(attrs={"type":"number", "class":"form-control", "placeholder":"Numero del empleado.", "min":"1"}),
            'nombrepila': forms.TextInput(attrs={"type":"text",'class': 'form-control', "placeholder":"Nombre."}),
            'primerapellido': forms.TextInput(attrs={"type":"text",'class': 'form-control', "placeholder":"Apelido paterno."}),
            'segundoapellido': forms.TextInput(attrs={"type":"text",'class': 'form-control', "placeholder":"Apellido materno."}),
            'calle': forms.TextInput(attrs={"type":"text",'class': 'form-control', "placeholder":"Calle."}),
            'numext': forms.TextInput(attrs={"type":"text", 'class': 'form-control', "placeholder":"Numero exterior."}),
            'colonia': forms.TextInput(attrs={"type":"text",'class': 'form-control', "placeholder":"Colonia."}),
            'numtel': forms.TextInput(attrs={"type":"text",'class': 'form-control', "placeholder":"Numero de telefono."}),
        }


class UpdateTransporte(forms.ModelForm):
    marca = forms.ModelChoiceField(queryset=Marca.objects.all(), widget=forms.Select(attrs={'class': 'form-select'}))
    modelo = forms.ModelChoiceField(queryset=Modelo.objects.all(), widget=forms.Select(attrs={'class': 'form-select'}))
    class Meta:
        model = Transportes
        fields = "__all__"
        widgets = {
            'noTransporte': forms.NumberInput(attrs={"type":"number", "class":"form-control", "placeholder":"Numero del empleado.", "min":"1"}),
            'matricula': forms.TextInput(attrs={"type":"text",'class': 'form-control', "placeholder":"Nombre."}),
            'cantasientos': forms.NumberInput(attrs={"type":"number",'class': 'form-control', "placeholder":"Cantidad de asientos."}),
            'disponibilidad': forms.TextInput(attrs={"type":"text",'class': 'form-control', "placeholder":"Disponibilidad."}),
            
        }


class CreateTransporte(forms.ModelForm):
    marca = forms.ModelChoiceField(queryset=Marca.objects.all(), widget=forms.Select(attrs={'class': 'form-select'}))
    modelo = forms.ModelChoiceField(queryset=Modelo.objects.all(), widget=forms.Select(attrs={'class': 'form-select'}))

    class Meta:
        model = Transportes
        fields = "__all__"
        exclude = ["disponibilidad", "cantasientos"]
        widgets = {
            'notransporte': forms.NumberInput(attrs={"type":"number", "class":"form-control", "placeholder":"Numero del empleado.", "min":"1"}),
            'matricula': forms.TextInput(attrs={"type":"text",'class': 'form-control', "placeholder":"Nombre."}),
            
            
        }


class UpdateParadas(forms.ModelForm):
    class Meta:
        model = Paradas
        fields = "__all__"
        widgets = {
            'clave': forms.TextInput(attrs={"type":"Text", "class":"form-control", "placeholder":"Clave.", "min":"1"}),
            'nombre': forms.TextInput(attrs={"type":"text",'class': 'form-control', "placeholder":"Nombre."}),
            'descripcion': forms.TextInput(attrs={"type":"text",'class': 'form-control', "placeholder":"Descripcion."}), 
        }


class CreateParadas(forms.ModelForm):
    class Meta:
        model = Paradas
        fields = "__all__"
        widgets = {
            'clave': forms.TextInput(attrs={"type":"Text", "class":"form-control", "placeholder":"Clave.", "min":"1"}),
            'nombre': forms.TextInput(attrs={"type":"text",'class': 'form-control', "placeholder":"Nombre."}),
            'descripcion': forms.TextInput(attrs={"type":"text",'class': 'form-control', "placeholder":"Descripcion."}), 
        }


class CreateConductorTransporte(forms.ModelForm):
    conductor = forms.ModelChoiceField(queryset=Conductores.objects.all(), widget=forms.Select(attrs={'class': 'form-select'}))
    transporte = forms.ModelChoiceField(queryset=Transportes.objects.all(), widget=forms.Select(attrs={'class': 'form-select'}))
    class Meta:
        model = ConductoresTransp
        fields = "__all__"
        widgets = {
            "fechainicio": DateInput(attrs={'type': 'date', 'class': 'form-control', 'placeholder': 'Fecha de inicio'}),
            "fechafinal": DateInput(attrs={'type': 'date', 'class': 'form-control', 'placeholder': 'Fecha final'}), 
        }


class CreateEmpleadoTransporte(forms.ModelForm):
    empleado = forms.ModelChoiceField(queryset=Empleados.objects.all(), widget=forms.Select(attrs={'class': 'form-select'}))
    transporte = forms.ModelChoiceField(queryset=Transportes.objects.all(), widget=forms.Select(attrs={'class': 'form-select'}))
    class Meta:
        model = EmpleadosTransp
        fields = "__all__"
        widgets = {
            "fechainicio": DateInput(attrs={'type': 'date', 'class': 'form-control', 'placeholder': 'Fecha de inicio'}),
            "fechafinal": DateInput(attrs={'type': 'date', 'class': 'form-control', 'placeholder': 'Fecha final'}), 
        }