from django.db.models.signals import post_save
from django.dispatch import receiver
from models import *

@receiver(post_save, sender=Transportes)
def asignar_disponibilidad_predeterminada(sender, instance, created, **kwargs):
    if created and not instance.disponibilidad:  
        default_disponibilidad = Disponibilidad.objects.get(clave= 1 )  
        instance.disponibilidad = default_disponibilidad
        instance.save()