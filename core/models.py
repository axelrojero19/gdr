from django.db import models
from django.contrib.auth.models import User
from datetime import timedelta
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.core.validators import MinValueValidator

#### GRANT GOAL ####
STATE_CHOICES = (
    ('Done', 'DN'),
    ('Doing', 'DG'),
    ('Not Stared', 'NS'),
)

class GrantGoal(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    gg_title = models.CharField(max_length=128, default="Generic Grant Goal Title")
    description = models.CharField(max_length=256, default="Generic Grant Goal Description")
    timestamp = models.DateField(auto_now_add=True)
    days_duration = models.IntegerField(default=7)
    final_date = models.DateField(auto_now_add=False, auto_now=False, blank=True, null=True)
    status = models.BooleanField(default=True)
    state = models.CharField(max_length=16, choices=STATE_CHOICES)
    sprint = models.IntegerField(default=1)
    slug = models.SlugField(max_length=16)

    def __str__(self):
        return self.gg_title

#############################################################################################################

class Administrador(models.Model):
    noempleado = models.IntegerField(db_column='NoEmpleado', primary_key=True,validators=[MinValueValidator(0)] )  # Field name made lowercase.
    nombrepila = models.CharField(db_column='NombrePila', max_length=25)  # Field name made lowercase.
    primerapellido = models.CharField(db_column='PrimerApellido', max_length=25)  # Field name made lowercase.
    segundoapellido = models.CharField(db_column='SegundoApellido', max_length=25, blank=True, null=True)  # Field name made lowercase.
    calle = models.CharField(db_column='Calle', max_length=30)  # Field name made lowercase.
    numext = models.CharField(db_column='NumExt', max_length=10)  # Field name made lowercase.
    colonia = models.CharField(db_column='Colonia', max_length=50)  # Field name made lowercase.
    numtel = models.CharField(db_column='NumTel', max_length=15)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Administrador'

    def __str__(self):
        return str(self.noempleado)

    @classmethod
    def create(cls, **kwargs):
        last_employee = cls.objects.order_by('-noempleado').first()
        new_employee_number = last_employee.noempleado + 1 if last_employee else 1

        new_admin = cls(noempleado=new_employee_number, **kwargs)
        new_admin.save()
        return new_admin

class Conductores(models.Model):
    noempleado = models.IntegerField(db_column='NoEmpleado', primary_key=True)  # Field name made lowercase.
    nombrepila = models.CharField(db_column='NombrePila', max_length=25)  # Field name made lowercase.
    primerapellido = models.CharField(db_column='PrimerApellido', max_length=25)  # Field name made lowercase.
    segundoapellido = models.CharField(db_column='SegundoApellido', max_length=25, blank=True, null=True)  # Field name made lowercase.
    numtel = models.CharField(db_column='NumTel', max_length=15)  # Field name made lowercase.
    administrador = models.ForeignKey(Administrador, models.DO_NOTHING, db_column='Administrador')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Conductores'

    def __str__(self):
        return str(self.noempleado)

class ConductoresTransp(models.Model):
    conductor = models.OneToOneField(Conductores, models.DO_NOTHING, db_column='Conductor', primary_key=True)  # Field name made lowercase. The composite primary key (Conductor, Transporte) found, that is not supported. The first column is selected.
    transporte = models.ForeignKey('Transportes', models.DO_NOTHING, db_column='Transporte')  # Field name made lowercase.
    fechainicio = models.DateField(db_column='FechaInicio')  # Field name made lowercase.
    fechafinal = models.DateField(db_column='FechaFinal', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Conductores_Transp'
        unique_together = (('conductor', 'transporte'),)


class ConductoresTurnos(models.Model):
    conductor = models.OneToOneField(Conductores, models.DO_NOTHING, db_column='Conductor', primary_key=True)  # Field name made lowercase. The composite primary key (Conductor, Turno) found, that is not supported. The first column is selected.
    turno = models.ForeignKey('Turnos', models.DO_NOTHING, db_column='Turno')  # Field name made lowercase.
    fechainicio = models.DateField(db_column='FechaInicio')  # Field name made lowercase.
    fechafinal = models.DateField(db_column='FechaFinal', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Conductores_Turnos'
        unique_together = (('conductor', 'turno'),)


class Disponibilidad(models.Model):
    clave = models.CharField(db_column='Clave', primary_key=True, max_length=1)  # Field name made lowercase.
    descripcion = models.CharField(db_column='Descripcion', max_length=10)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Disponibilidad'

    def __str__(self):
        return self.descripcion

class Empleados(models.Model):
    noempleado = models.IntegerField(db_column='NoEmpleado', primary_key=True)  # Field name made lowercase.
    nombrepila = models.CharField(db_column='NombrePila', max_length=25)  # Field name made lowercase.
    primerapellido = models.CharField(db_column='PrimerApellido', max_length=25)  # Field name made lowercase.
    segundoapellido = models.CharField(db_column='SegundoApellido', max_length=25, blank=True, null=True)  # Field name made lowercase.
    calle = models.CharField(db_column='Calle', max_length=30)  # Field name made lowercase.
    numext = models.CharField(db_column='NumExt', max_length=10)  # Field name made lowercase.
    colonia = models.CharField(db_column='Colonia', max_length=50)  # Field name made lowercase.
    numtel = models.CharField(db_column='NumTel', max_length=15)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Empleados'

    def __str__(self):
        return str(self.noempleado)

class EmpleadosTransp(models.Model):
    empleado = models.OneToOneField(Empleados, models.DO_NOTHING, db_column='Empleado', primary_key=True)  # Field name made lowercase. The composite primary key (Empleado, Transporte) found, that is not supported. The first column is selected.
    transporte = models.ForeignKey('Transportes', models.DO_NOTHING, db_column='Transporte')  # Field name made lowercase.
    fechainicio = models.DateField(db_column='FechaInicio')  # Field name made lowercase.
    fechafinal = models.DateField(db_column='FechaFinal', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Empleados_Transp'
        unique_together = (('empleado', 'transporte'),)


class Marca(models.Model):
    codigo = models.CharField(db_column='Codigo', primary_key=True, max_length=5)  # Field name made lowercase.
    nombre = models.CharField(db_column='Nombre', max_length=15)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Marca'

    def __str__(self):
        return self.nombre

class Modelo(models.Model):
    codigo = models.CharField(db_column='Codigo', primary_key=True, max_length=5)  # Field name made lowercase.
    nombre = models.CharField(db_column='Nombre', max_length=20)  # Field name made lowercase.
    ano = models.IntegerField(db_column='Ano')  # Field name made lowercase.
    marca = models.ForeignKey(Marca, models.DO_NOTHING, db_column='Marca')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Modelo'

    def __str__(self):
        return self.nombre

class Paradas(models.Model):
    clave = models.CharField(db_column='Clave', primary_key=True, max_length=5)  # Field name made lowercase.
    nombre = models.CharField(db_column='Nombre', max_length=100)  # Field name made lowercase.
    descripcion = models.CharField(db_column='Descripcion', max_length=100)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Paradas'

    def __str__(self):
        return self.nombre

class Rutas(models.Model):
    numeroconsecutivo = models.IntegerField(db_column='Numeroconsecutivo', primary_key=True)  # Field name made lowercase.
    noruta = models.IntegerField(db_column='NoRuta', blank=True, null=True)  # Field name made lowercase.
    conductor = models.ForeignKey(Conductores, models.DO_NOTHING, db_column='Conductor', blank=True, null=True)  # Field name made lowercase.
    transporte = models.ForeignKey('Transportes', models.DO_NOTHING, db_column='Transporte', blank=True, null=True)  # Field name made lowercase.
    parada = models.ForeignKey(Paradas, models.DO_NOTHING, db_column='Parada', blank=True, null=True)  # Field name made lowercase.
    nombre = models.CharField(db_column='Nombre', max_length=15, blank=True, null=True)  # Field name made lowercase.
    hora = models.CharField(db_column='Hora', max_length=5)  # Field name made lowercase.
    fechainicio = models.DateField(db_column='FechaInicio')  # Field name made lowercase.
    fechafinal = models.DateField(db_column='FechaFinal', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Rutas'

class Transportes(models.Model):
    notransporte = models.IntegerField(db_column='NoTransporte', primary_key=True)  # Field name made lowercase.
    matricula = models.CharField(db_column='Matricula', unique=True, max_length=10)  # Field name made lowercase.
    cantasientos = models.IntegerField(db_column='CantAsientos', default=41)  # Field name made lowercase.
    marca = models.ForeignKey(Marca, models.DO_NOTHING, db_column='Marca')  # Field name made lowercase.
    modelo = models.ForeignKey(Modelo, models.DO_NOTHING, db_column='Modelo')  # Field name made lowercase.
    disponibilidad = models.ForeignKey(Disponibilidad, models.DO_NOTHING, db_column='Disponibilidad', default=1) # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Transportes'

    def __str__(self):
        return str(self.notransporte)


class Turnos(models.Model):
    clave = models.CharField(db_column='Clave', primary_key=True, max_length=5)  # Field name made lowercase.
    descripcion = models.CharField(db_column='Descripcion', max_length=25)  # Field name made lowercase.
    horario = models.CharField(db_column='Horario', max_length=25)  # Field name made lowercase.
    administrador = models.ForeignKey(Administrador, models.DO_NOTHING, db_column='Administrador', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Turnos'

    def __str__(self):
        return self.clave

class TurnosParadas(models.Model):
    turno = models.OneToOneField(Turnos, models.DO_NOTHING, db_column='Turno', primary_key=True)  # Field name made lowercase. The composite primary key (Turno, Parada) found, that is not supported. The first column is selected.
    parada = models.ForeignKey(Paradas, models.DO_NOTHING, db_column='Parada')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Turnos_Paradas'
        unique_together = (('turno', 'parada'),)


class AuthGroup(models.Model):
    name = models.CharField(unique=True, max_length=150)

    class Meta:
        managed = False
        db_table = 'auth_group'


class AuthGroupPermissions(models.Model):
    id = models.BigAutoField(primary_key=True)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)
    permission = models.ForeignKey('AuthPermission', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_group_permissions'
        unique_together = (('group', 'permission'),)


class AuthPermission(models.Model):
    name = models.CharField(max_length=255)
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING)
    codename = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'auth_permission'
        unique_together = (('content_type', 'codename'),)


class AuthUser(models.Model):
    password = models.CharField(max_length=128)
    last_login = models.DateTimeField(blank=True, null=True)
    is_superuser = models.IntegerField()
    username = models.CharField(unique=True, max_length=150)
    first_name = models.CharField(max_length=150)
    last_name = models.CharField(max_length=150)
    email = models.CharField(max_length=254)
    is_staff = models.IntegerField()
    is_active = models.IntegerField()
    date_joined = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'auth_user'


class AuthUserGroups(models.Model):
    id = models.BigAutoField(primary_key=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_groups'
        unique_together = (('user', 'group'),)


class AuthUserUserPermissions(models.Model):
    id = models.BigAutoField(primary_key=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    permission = models.ForeignKey(AuthPermission, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_user_permissions'
        unique_together = (('user', 'permission'),)


class CoreGrantgoal(models.Model):
    id = models.BigAutoField(primary_key=True)
    gg_title = models.CharField(max_length=128)
    description = models.CharField(max_length=256)
    timestamp = models.DateField()
    days_duration = models.IntegerField()
    final_date = models.DateField(blank=True, null=True)
    status = models.IntegerField()
    state = models.CharField(max_length=16)
    sprint = models.IntegerField()
    slug = models.CharField(max_length=16)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'core_grantgoal'


class CoreRoutes(models.Model):
    id = models.BigAutoField(primary_key=True)
    route_name = models.CharField(max_length=100)
    start_date = models.DateField()
    final_date = models.DateField(blank=True, null=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'core_routes'


class CoreStop(models.Model):
    id = models.BigAutoField(primary_key=True)
    name = models.CharField(max_length=100)
    code = models.IntegerField()
    description = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'core_stop'


class DjangoAdminLog(models.Model):
    action_time = models.DateTimeField()
    object_id = models.TextField(blank=True, null=True)
    object_repr = models.CharField(max_length=200)
    action_flag = models.PositiveSmallIntegerField()
    change_message = models.TextField()
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING, blank=True, null=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'django_admin_log'


class DjangoContentType(models.Model):
    app_label = models.CharField(max_length=100)
    model = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'django_content_type'
        unique_together = (('app_label', 'model'),)


class DjangoMigrations(models.Model):
    id = models.BigAutoField(primary_key=True)
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'


class DjangoSession(models.Model):
    session_key = models.CharField(primary_key=True, max_length=40)
    session_data = models.TextField()
    expire_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_session'