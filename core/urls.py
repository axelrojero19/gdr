from django.urls import path
from core import views
from .views import NuevaTuplaRuta

app_name = "core"

urlpatterns = [
    path('list/grantgoal/', views.ListGrantGoal.as_view(), name="gg_list"),
    path('create/grantgoal/', views.CreateGrantGoal.as_view(), name="gg_create"),
    path('detail/grantgoal/<int:pk>/', views.DetailGrantGoal.as_view(), name="gg_detail"),
    path('update/grantgoal/<int:pk>/', views.UpdateGrantGoal.as_view(), name="gg_update"),
    path('list/routes/', views.ListRoutes.as_view(), name="list_routes"),
    path('create/route/', views.CreateRoute.as_view(), name="route_create"),
    path('route/<int:noruta>/', views.DetailRoute.as_view(), name="route_detail"),
    path('stop/route/<int:numeroconsecutivo>/', views.AddStop.as_view(), name="route_stop"),
    path('delete/route/<int:noruta>', views.DeleteRoute.as_view(), name="route_delete"),
    path('stop/delete/<int:pk>/', views.DeleteStop.as_view(), name="route_stopd"),
    path("list/empleados/", views.ListEmpleados.as_view(), name="list_empleados"),
    path('empleados/<int:pk>/', views.DetailEmpleados.as_view(), name='empleados_detail'),
    path('create/empleado/', views.CreateEmpleado.as_view(), name="empleados_create"),
    path('update/empleado/<int:pk>/', views.UpdateEmpleado.as_view(), name="empleados_update"),
    path('delete/empleados/<int:pk>/', views.DeleteEmpleado.as_view(), name="empleados_delete"),
    path("list/conductores/", views.ListConductores.as_view(), name="list_conductores"),
    path('conductores/<int:pk>/', views.DetailConductores.as_view(), name='conductores_detail'),
    path('create/conductor/', views.CreateConductores.as_view(), name="conductores_create"),
    path('update/conductor/<int:pk>/', views.UpdateConductores.as_view(), name="conductores_update"),
    path('delete/conductores/<int:pk>/', views.DeleteConductores.as_view(), name="conductores_delete"),
    path("list/administradores/", views.ListAdministradores.as_view(), name="list_administradores"),
    path('administradores/<int:pk>/', views.DetailAdministrador.as_view(), name='administrador_detail'),
    path('create/administrador/', views.CreateAdministrador.as_view(), name="administrador_create"),
    path('update/administrador/<int:pk>/', views.UpdateAdministrador.as_view(), name="administrador_update"),
    path('delete/administrador/<int:pk>/', views.DeleteAdminstrador.as_view(), name="administrador_delete"),
    path("list/transportes/", views.ListTransportes.as_view(), name="list_transportes"),
    path('transportes/<int:pk>/', views.DetailTrasporte.as_view(), name='transporte_detail'),
    path('create/transporte/', views.CreateTransporte.as_view(), name="transporte_create"),
    path('update/transporte/<int:pk>/', views.UpdateTransporte.as_view(), name="transporte_update"),
    path('delete/transporte/<int:pk>/', views.DeleteTransporte.as_view(), name="transporte_delete"),
    path("list/paradas/", views.ListParadas.as_view(), name="list_paradas"),
    path('paradas/<int:pk>/', views.DetailParada.as_view(), name= "parada_detail"),
    path('create/paradas/', views.CreateParada.as_view(), name="parada_create"),
    path('update/paradas/<int:pk>/', views.UpdateParada.as_view(), name="parada_update"),
    path('delete/paradas/<int:pk>/', views.DeleteParada.as_view(), name="parada_delete"),
    path('list/transporte/conductor', views.ListConductoresTransportes.as_view(), name="list_conductorestransportes"),
    path('create/transporte/conductor', views.CreateConductorTransporte.as_view(), name="conductortransporte_create"),
    path('delete/transporte/conductor/<int:pk>/', views.DeleteConductorTransporte.as_view(), name="conductortransporte_delete"),
    path('create/transporte/empleado', views.CreateEmpleadoTransporte.as_view(), name="empleadotransporte_create"),
    path('delete/transporte/empleado/<int:pk>/', views.DeleteEmpleadoTransporte.as_view(), name="empleadotransporte_delete"),
    path('list/transporte/empleado', views.ListEmpleadosTransportes.as_view(), name="list_empleadostransportes"),
    path('crear_tupla/<int:id_tupla>/', NuevaTuplaRuta.as_view(), name='crear_tupla'),
   
]


