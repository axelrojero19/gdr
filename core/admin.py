from django.contrib import admin

# Register your models here.
from .models import *

@admin.register(GrantGoal)
class GrantGoalAdmin(admin.ModelAdmin):
    list_display = [
        "gg_title",
        "timestamp",
        "days_duration",
        "final_date",
        "user"
    ]


@admin.register(Administrador)
class AdministradorAdmin(admin.ModelAdmin):
    list_display = [
        "noempleado",
        "nombrepila",
        "primerapellido",
        "segundoapellido",
    ]


@admin.register(Conductores)
class ConductoresAdmin(admin.ModelAdmin):
    list_display = [
        "noempleado",
        "nombrepila",
        "primerapellido",
        "segundoapellido",
        "numtel",
        "administrador",
    ]


@admin.register(ConductoresTransp)
class ConductoresTranspAdmin(admin.ModelAdmin):
    list_display = [
        "conductor",
        "transporte",
        "fechainicio",
        "fechafinal",
    ]


@admin.register(ConductoresTurnos)
class ConductoresTurnosAdmin(admin.ModelAdmin):
    list_display = [
        "conductor",
        "turno",
        "fechainicio",
        "fechafinal",
    ]


@admin.register(Disponibilidad)
class DisponibilidadAdmin(admin.ModelAdmin):
    list_display = [
        "clave",
        "descripcion",
    ]


@admin.register(Empleados)
class EmpleadosAdmin(admin.ModelAdmin):
    list_display = [
        "noempleado",
        "nombrepila",
        "primerapellido",
        "segundoapellido",
        "calle",
        "numext",
        "colonia",
        "numtel",
    ]


@admin.register(EmpleadosTransp)
class EmpleadosTranspAdmin(admin.ModelAdmin):
    list_display = [
        "empleado",
        "transporte",
        "fechainicio",
        "fechafinal",
    ]


@admin.register(Marca)
class MarcaAdmin(admin.ModelAdmin):
    list_display = [
        "codigo",
        "nombre",
    ]


@admin.register(Modelo)
class ModeloAdmin(admin.ModelAdmin):
    list_display = [
        "codigo",
        "nombre",
        "ano",
        "marca",
    ]


@admin.register(Paradas)
class ParadasAdmin(admin.ModelAdmin):
    list_display = [
        "clave",
        "nombre",
        "descripcion",
    ]


@admin.register(Rutas)
class RutasAdmin(admin.ModelAdmin):
    list_display = [
        "numeroconsecutivo",
        "noruta",
        "conductor",
        "transporte",
        "parada",
        "nombre",
        "hora",
        "fechainicio",
        "fechafinal",
    ]


@admin.register(Transportes)
class TransportesAdmin(admin.ModelAdmin):
    list_display = [
        "notransporte",
        "matricula",
        "cantasientos",
        "marca",
        "modelo",
        "disponibilidad",
    ]


@admin.register(Turnos)
class TurnosAdmin(admin.ModelAdmin):
    list_display = [
        "clave",
        "descripcion",
        "horario",
        "administrador",
    ]


@admin.register(TurnosParadas)
class TurnosParadasAdmin(admin.ModelAdmin):
    list_display = [
        "turno",
        "parada",
    ]