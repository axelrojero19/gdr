from django.db import connection
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect
from django.views import generic
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy, reverse
from .forms import *
from .models import UserProfile
from django.views.generic import FormView

class SignUp(generic.CreateView):
    template_name = "home/signup.html"
    form_class = SignUpForm
    #success_url = reverse_lazy("home:index")

    def form_valid(self, form):
        user = form.save()
        username = form.cleaned_data.get("username")
        password1 = form.cleaned_data.get("password1")
        user = authenticate(self.request, username=username, password=password1)
        if user is not None:
            login(self.request, user)
        return HttpResponseRedirect(self.get_success_url(), user.id)

    def get_success_url(self) -> str:
        pk = self.kwargs[pk]
        return reverse("home:detail_userprofile", kwargs={"pk": pk})    
    


class DetailUserProfile(LoginRequiredMixin, generic.View):
    template_name = "home/detail_userprofile.html"
    context = {}
    login_url = "/"

    def get(self, request, pk):
        self.context = {
            "profile": UserProfile.objects.get(id=pk)
        }
        return render(request, self.template_name,self.context)
    
class UpdateUserProfile(LoginRequiredMixin, generic.UpdateView):
    template_name = "home/update_userprofile.html"
    model = UserProfile
    form_class = UserProfileForm
    success_url = reverse_lazy("home:detail_userprofile")
    login_url = "/"

    def get_success_url(self):
        pk = self.kwargs["pk"]
        return reverse("home:detail_userprofile", kwargs={"pk":pk})



class Index(generic.View):
    template_name = "home/index.html"
    context = {}

    def get(self, request, *args, **kwargs):
        self.context = {
            "name": "Enrique Flores"
        }
        return render(request, self.template_name, self.context)
    
    def post(self, request):
        username = request.POST["username"]
        password = request.POST["password"]
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect("/")
        else:
            return redirect("/")

    

class About(generic.View):
    template_name = "home/about.html"
    context = {}

    def get(self, request, *args, **kwargs):
        self.context = {
            "name": "Enrique Flores"
        }
        return render(request, self.template_name, self.context)


class Identity(generic.View):
    template_name = "home/identity.html"
    context = {}

    def get(self, request, *args, **kwargs):
        self.context = {
            "name": "Enrique Flores"
        }
        return render(request, self.template_name, self.context)


class Contact(generic.View):
    template_name = "home/contact.html"
    context = {}

    def get(self, request, *args, **kwargs):
        self.context = {
            "name": "Enrique Flores"
        }
        return render(request, self.template_name, self.context)
    
class Rutas(generic.View):
    template_name = "home/rutas.html"
    context = {}

    def get(self, request, *args, **kwargs):
        self.context = {
            "name": "Enrique Flores"
        }
        return render(request, self.template_name, self.context)
    
class Empleados(generic.View):
    template_name = "home/forms.html"
    context = {}

    def get(self, request, *args, **kwargs):
        self.context = {
            "name": "Enrique Flores"
        }
        return render(request, self.template_name, self.context)
    
class LogOut(generic.View):
    def get(self, request):
        logout(request)
        return redirect("/")



class SignUp(generic.CreateView):
    template_name = "home/signup.html"
    form_class = SignUpForm
    success_url = reverse_lazy("home:index")

    def form_valid(self, form):
        form.save()
        username = form.cleaned_data.get("username")
        password1 = form.cleaned_data.get("password1")
        user = authenticate(self.request, username=username, password=password1)
        if user is not None:
            login(self.request, user)
        return redirect("/")

class CustomLoginView(FormView):
    template_name = 'registration/login.html'
    form_class = LoginForm  # Utiliza tu formulario de inicio de sesión
    success_url = reverse_lazy('home:index')

    def form_valid(self, form):
        # Lógica adicional después del inicio de sesión exitoso
        user = authenticate(
            self.request,
            username=form.cleaned_data['username'],
            password=form.cleaned_data['password']
        )

        if user is not None:
            login(self.request, user)

        return redirect("/")



class CreateUserProfile(LoginRequiredMixin, generic.CreateView):
    template_name = "home/user_profile.html"
    model = UserProfile
    form_class = UserProfileForm
    success_url = reverse_lazy("home:user_profile")
    login_url = "/"


class Formal(generic.View):
    template_name = "home/formal.html"
    context = {}

    def get(self, request, *args, **kwargs):
        self.context = {
            "name": "Enrique Flores"
        }
        return render(request, self.template_name, self.context)


class Polo(generic.View):
    template_name = "home/polo.html"
    context = {}

    def get(self, request, *args, **kwargs):
        self.context = {
            "name": "Enrique Flores"
        }
        return render(request, self.template_name, self.context)

class Tazas(generic.View):
    template_name = "home/tazas.html"
    context = {}

    def get(self, request, *args, **kwargs):
        self.context = {
            "name": "Enrique Flores"
        }
        return render(request, self.template_name, self.context)
    
class Gorras(generic.View):
    template_name = "home/gorras.html"
    context = {}

    def get(self, request, *args, **kwargs):
        self.context = {
            "name": "Enrique Flores"
        }
        return render(request, self.template_name, self.context)
    
class Unidades(generic.View):
    template_name = "home/unidades.html"
    context = {}

    def get(self, request, *args, **kwargs):
        self.context = {
            "name": "Enrique Flores"
        }
        return render(request, self.template_name, self.context)
    
    
def is_superuser(user):
    return user.is_superuser



# Consultas

# views.py

class Horarios1(generic.TemplateView):
    template_name = "home/horarios1.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        with connection.cursor() as cursor:
            cursor.execute("""
                select DISTINCT
                    p.Nombre as Paradas,
                    p.Descripcion as Descripcion
                    from Rutas as r
                    inner join Paradas as p on r.Parada = p.Clave
                    where r.`Nombre` = "5 y 10"
            """)

            rutas_data = cursor.fetchall()

        context['rutas1'] = rutas_data

        with connection.cursor() as cursor:
            cursor.execute("""
                select
                Hora
                from Rutas
                where `NoRuta` = 101
            """)

            rutas_data2 = cursor.fetchall()

        context['rutas11'] = rutas_data2

        with connection.cursor() as cursor:
            cursor.execute("""
                select
                Hora
                from Rutas
                where `NoRuta` = 104
            """)

            rutas_data3 = cursor.fetchall()

        context['rutas111'] = rutas_data3
        
        with connection.cursor() as cursor:
            cursor.execute("""
                select
                Hora
                from Rutas
                where `NoRuta` = 107
            """)

            rutas_data4 = cursor.fetchall()

        context['rutas1111'] = rutas_data4

        return context


    
    
    
    
    

class Horarios2(generic.TemplateView):
    template_name = "home/horarios2.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        with connection.cursor() as cursor:
            cursor.execute("""
                select DISTINCT
                    p.Nombre as Paradas,
                    p.Descripcion as Descripcion
                    from Rutas as r
                    inner join Paradas as p on r.Parada = p.Clave
                    where r.`Nombre` = "Natura"
            """)

            rutas_data = cursor.fetchall()

        context['rutas2'] = rutas_data

        with connection.cursor() as cursor:
            cursor.execute("""
                select
                Hora
                from Rutas
                where `NoRuta` = 102
            """)

            rutas_data2 = cursor.fetchall()

        context['rutas22'] = rutas_data2

        with connection.cursor() as cursor:
            cursor.execute("""
                select
                Hora
                from Rutas
                where `NoRuta` = 105
            """)

            rutas_data3 = cursor.fetchall()

        context['rutas222'] = rutas_data3
        
        with connection.cursor() as cursor:
            cursor.execute("""
                select
                Hora
                from Rutas
                where `NoRuta` = 108
            """)

            rutas_data4 = cursor.fetchall()

        context['rutas2222'] = rutas_data4

        return context



class Horarios3(generic.TemplateView):
    template_name = "home/horarios3.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        with connection.cursor() as cursor:
            cursor.execute("""
                select DISTINCT
                    p.Nombre as Paradas,
                    p.Descripcion as Descripcion
                    from Rutas as r
                    inner join Paradas as p on r.Parada = p.Clave
                    where r.`Nombre` = "Villa Fontana"
            """)

            rutas_data = cursor.fetchall()

        context['rutas3'] = rutas_data

        with connection.cursor() as cursor:
            cursor.execute("""
                select
                Hora
                from Rutas
                where `NoRuta` = 103
            """)

            rutas_data2 = cursor.fetchall()

        context['rutas33'] = rutas_data2

        with connection.cursor() as cursor:
            cursor.execute("""
                select
                Hora
                from Rutas
                where `NoRuta` = 106
            """)

            rutas_data3 = cursor.fetchall()

        context['rutas333'] = rutas_data3
        
        with connection.cursor() as cursor:
            cursor.execute("""
                select
                Hora
                from Rutas
                where `NoRuta` = 109
            """)

            rutas_data4 = cursor.fetchall()

        context['rutas3333'] = rutas_data4

        return context
