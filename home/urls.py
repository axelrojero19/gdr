from django.urls import path
from home import views
from .views import *

app_name = "home"

urlpatterns = [
    path('', views.Index.as_view(), name="index"),
    path('login/', CustomLoginView.as_view(), name='login'),    
    path('signup/', views.SignUp.as_view(), name="signup"),
    path('logout/', views.LogOut.as_view(), name="logout"),
    path('about/', views.About.as_view(), name="about"),    
    path('identity/', views.Identity.as_view(), name="identity"),
    path('contact/', views.Contact.as_view(), name="contact"),
    path('rutas/', views.Rutas.as_view(), name="rutas"),
    path('horarios1/', views.Horarios1.as_view(), name="horarios1"),
    path('horarios2/', views.Horarios2.as_view(), name="horarios2"),
    path('horarios3/', views.Horarios3.as_view(), name="horarios3"),
    path('empleados/', views.Empleados.as_view(), name="empleados"),
    path('formal/', views.Formal.as_view(), name="formal"),
    path('polo/', views.Polo.as_view(), name="polo"),
    path('tazas/', views.Tazas.as_view(), name="tazas"),
    path('gorras/', views.Gorras.as_view(), name="gorras"),
    path('unidades/', views.Unidades.as_view(), name="unidades"),
    path('horarios1/', Horarios1.as_view(), name='horarios1'),
    path('horarios2/', Horarios2.as_view(), name='horarios2'),
    path('horarios3/', Horarios3.as_view(), name='horarios3'),
]
