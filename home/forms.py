from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django import forms
from .models import UserProfile  

#### SIGNUP ####

class SignUpForm(UserCreationForm):
    username = forms.CharField(label='Nombre de usuario', max_length=32, widget=forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escriba su nombre de usuario"}))
    password1 = forms.CharField(label='Ingrese su contraseña', max_length=32, widget=forms.PasswordInput(attrs={"type":"password", "class":"form-control", "placeholder":"Escriba su contraseña"}))
    password2 = forms.CharField(label='Ingrese su contraseña nuevamente', max_length=32, widget=forms.PasswordInput(attrs={"type":"password", "class":"form-control", "placeholder":"Repita su contraseña"}))
    email = forms.CharField(label='Correo electronico', max_length=32, widget=forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escriba tu email"}))
    first_name = forms.CharField(label='Nombre', max_length=32, widget=forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escriba su nombre"}))
    last_name = forms.CharField(label='Apellido', max_length=32, widget=forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escriba su apellido"}))
    class Meta:
        model = User
        fields = [
            "username",
            "password1",
            "password2",
            "email",
            "first_name",
            "last_name"
           
        ]

#### LOGIN ####

class LoginForm(forms.Form):
    username = forms.CharField(label='Nombre de usuario', max_length=32, widget=forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escriba su nombre de usuario"}))
    password = forms.CharField(label='Ingrese su contraseña', max_length=32, widget=forms.PasswordInput(attrs={"type":"password", "class":"form-control", "placeholder":"Escriba su contraseña"}))

#### USER PROFILE ####

class UserProfileForm(forms.ModelForm):
    class Meta:
        model = UserProfile
        fields = [
            "user",
            "bio",
            "status"
        ]
        widgets = {
            "user": forms.Select(attrs={"type":"select", "class":"form-select"}),
            "bio": forms.Textarea(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe acerca de ti", "row":3}),
            "status": forms.CheckboxInput(attrs={"type":"checkbox", "class":"form-checkbox"}),
        }