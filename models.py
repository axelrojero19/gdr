# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class Administrador(models.Model):
    noempleado = models.IntegerField(db_column='NoEmpleado', primary_key=True)  # Field name made lowercase.
    nombrepila = models.CharField(db_column='NombrePila', max_length=25)  # Field name made lowercase.
    primerapellido = models.CharField(db_column='PrimerApellido', max_length=25)  # Field name made lowercase.
    segundoapellido = models.CharField(db_column='SegundoApellido', max_length=25, blank=True, null=True)  # Field name made lowercase.
    calle = models.CharField(db_column='Calle', max_length=30)  # Field name made lowercase.
    numext = models.CharField(db_column='NumExt', max_length=10)  # Field name made lowercase.
    colonia = models.CharField(db_column='Colonia', max_length=50)  # Field name made lowercase.
    numtel = models.CharField(db_column='NumTel', max_length=15)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Administrador'


class Conductores(models.Model):
    noempleado = models.IntegerField(db_column='NoEmpleado', primary_key=True)  # Field name made lowercase.
    nombrepila = models.CharField(db_column='NombrePila', max_length=25)  # Field name made lowercase.
    primerapellido = models.CharField(db_column='PrimerApellido', max_length=25)  # Field name made lowercase.
    segundoapellido = models.CharField(db_column='SegundoApellido', max_length=25, blank=True, null=True)  # Field name made lowercase.
    numtel = models.CharField(db_column='NumTel', max_length=15)  # Field name made lowercase.
    administrador = models.ForeignKey(Administrador, models.DO_NOTHING, db_column='Administrador')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Conductores'


class ConductoresTransp(models.Model):
    conductor = models.OneToOneField(Conductores, models.DO_NOTHING, db_column='Conductor', primary_key=True)  # Field name made lowercase. The composite primary key (Conductor, Transporte) found, that is not supported. The first column is selected.
    transporte = models.ForeignKey('Transportes', models.DO_NOTHING, db_column='Transporte')  # Field name made lowercase.
    fechainicio = models.DateField(db_column='FechaInicio')  # Field name made lowercase.
    fechafinal = models.DateField(db_column='FechaFinal', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Conductores_Transp'
        unique_together = (('conductor', 'transporte'),)


class ConductoresTurnos(models.Model):
    conductor = models.OneToOneField(Conductores, models.DO_NOTHING, db_column='Conductor', primary_key=True)  # Field name made lowercase. The composite primary key (Conductor, Turno) found, that is not supported. The first column is selected.
    turno = models.ForeignKey('Turnos', models.DO_NOTHING, db_column='Turno')  # Field name made lowercase.
    fechainicio = models.DateField(db_column='FechaInicio')  # Field name made lowercase.
    fechafinal = models.DateField(db_column='FechaFinal', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Conductores_Turnos'
        unique_together = (('conductor', 'turno'),)


class Disponibilidad(models.Model):
    clave = models.CharField(db_column='Clave', primary_key=True, max_length=1)  # Field name made lowercase.
    descripcion = models.CharField(db_column='Descripcion', max_length=10)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Disponibilidad'


class Empleados(models.Model):
    noempleado = models.IntegerField(db_column='NoEmpleado', primary_key=True)  # Field name made lowercase.
    nombrepila = models.CharField(db_column='NombrePila', max_length=25)  # Field name made lowercase.
    primerapellido = models.CharField(db_column='PrimerApellido', max_length=25)  # Field name made lowercase.
    segundoapellido = models.CharField(db_column='SegundoApellido', max_length=25, blank=True, null=True)  # Field name made lowercase.
    calle = models.CharField(db_column='Calle', max_length=30)  # Field name made lowercase.
    numext = models.CharField(db_column='NumExt', max_length=10)  # Field name made lowercase.
    colonia = models.CharField(db_column='Colonia', max_length=50)  # Field name made lowercase.
    numtel = models.CharField(db_column='NumTel', max_length=15)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Empleados'


class EmpleadosTransp(models.Model):
    empleado = models.OneToOneField(Empleados, models.DO_NOTHING, db_column='Empleado', primary_key=True)  # Field name made lowercase. The composite primary key (Empleado, Transporte) found, that is not supported. The first column is selected.
    transporte = models.ForeignKey('Transportes', models.DO_NOTHING, db_column='Transporte')  # Field name made lowercase.
    fechainicio = models.DateField(db_column='FechaInicio')  # Field name made lowercase.
    fechafinal = models.DateField(db_column='FechaFinal', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Empleados_Transp'
        unique_together = (('empleado', 'transporte'),)


class Marca(models.Model):
    codigo = models.CharField(db_column='Codigo', primary_key=True, max_length=5)  # Field name made lowercase.
    nombre = models.CharField(db_column='Nombre', max_length=15)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Marca'


class Modelo(models.Model):
    codigo = models.CharField(db_column='Codigo', primary_key=True, max_length=5)  # Field name made lowercase.
    nombre = models.CharField(db_column='Nombre', max_length=20)  # Field name made lowercase.
    ano = models.IntegerField(db_column='Ano')  # Field name made lowercase.
    marca = models.ForeignKey(Marca, models.DO_NOTHING, db_column='Marca')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Modelo'


class Paradas(models.Model):
    clave = models.CharField(db_column='Clave', primary_key=True, max_length=5)  # Field name made lowercase.
    nombre = models.CharField(db_column='Nombre', max_length=100)  # Field name made lowercase.
    descripcion = models.CharField(db_column='Descripcion', max_length=100)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Paradas'


class Rutas(models.Model):
    numeroconsecutivo = models.IntegerField(db_column='Numeroconsecutivo', primary_key=True)  # Field name made lowercase.
    noruta = models.IntegerField(db_column='NoRuta', blank=True, null=True)  # Field name made lowercase.
    conductor = models.ForeignKey(Conductores, models.DO_NOTHING, db_column='Conductor', blank=True, null=True)  # Field name made lowercase.
    transporte = models.ForeignKey('Transportes', models.DO_NOTHING, db_column='Transporte', blank=True, null=True)  # Field name made lowercase.
    parada = models.ForeignKey(Paradas, models.DO_NOTHING, db_column='Parada', blank=True, null=True)  # Field name made lowercase.
    nombre = models.CharField(db_column='Nombre', max_length=15, blank=True, null=True)  # Field name made lowercase.
    hora = models.CharField(db_column='Hora', max_length=5)  # Field name made lowercase.
    fechainicio = models.DateField(db_column='FechaInicio')  # Field name made lowercase.
    fechafinal = models.DateField(db_column='FechaFinal', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Rutas'


class Transportes(models.Model):
    notransporte = models.IntegerField(db_column='NoTransporte', primary_key=True)  # Field name made lowercase.
    matricula = models.CharField(db_column='Matricula', unique=True, max_length=10)  # Field name made lowercase.
    cantasientos = models.IntegerField(db_column='CantAsientos')  # Field name made lowercase.
    marca = models.ForeignKey(Marca, models.DO_NOTHING, db_column='Marca')  # Field name made lowercase.
    modelo = models.ForeignKey(Modelo, models.DO_NOTHING, db_column='Modelo')  # Field name made lowercase.
    disponibilidad = models.ForeignKey(Disponibilidad, models.DO_NOTHING, db_column='Disponibilidad')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Transportes'


class Turnos(models.Model):
    clave = models.CharField(db_column='Clave', primary_key=True, max_length=5)  # Field name made lowercase.
    descripcion = models.CharField(db_column='Descripcion', max_length=25)  # Field name made lowercase.
    horario = models.CharField(db_column='Horario', max_length=25)  # Field name made lowercase.
    administrador = models.ForeignKey(Administrador, models.DO_NOTHING, db_column='Administrador', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Turnos'


class TurnosParadas(models.Model):
    turno = models.OneToOneField(Turnos, models.DO_NOTHING, db_column='Turno', primary_key=True)  # Field name made lowercase. The composite primary key (Turno, Parada) found, that is not supported. The first column is selected.
    parada = models.ForeignKey(Paradas, models.DO_NOTHING, db_column='Parada')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Turnos_Paradas'
        unique_together = (('turno', 'parada'),)


class AuthGroup(models.Model):
    name = models.CharField(unique=True, max_length=150)

    class Meta:
        managed = False
        db_table = 'auth_group'


class AuthGroupPermissions(models.Model):
    id = models.BigAutoField(primary_key=True)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)
    permission = models.ForeignKey('AuthPermission', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_group_permissions'
        unique_together = (('group', 'permission'),)


class AuthPermission(models.Model):
    name = models.CharField(max_length=255)
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING)
    codename = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'auth_permission'
        unique_together = (('content_type', 'codename'),)


class AuthUser(models.Model):
    password = models.CharField(max_length=128)
    last_login = models.DateTimeField(blank=True, null=True)
    is_superuser = models.IntegerField()
    username = models.CharField(unique=True, max_length=150)
    first_name = models.CharField(max_length=150)
    last_name = models.CharField(max_length=150)
    email = models.CharField(max_length=254)
    is_staff = models.IntegerField()
    is_active = models.IntegerField()
    date_joined = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'auth_user'


class AuthUserGroups(models.Model):
    id = models.BigAutoField(primary_key=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_groups'
        unique_together = (('user', 'group'),)


class AuthUserUserPermissions(models.Model):
    id = models.BigAutoField(primary_key=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)
    permission = models.ForeignKey(AuthPermission, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'auth_user_user_permissions'
        unique_together = (('user', 'permission'),)


class CoreGrantgoal(models.Model):
    id = models.BigAutoField(primary_key=True)
    gg_title = models.CharField(max_length=128)
    description = models.CharField(max_length=256)
    timestamp = models.DateField()
    days_duration = models.IntegerField()
    final_date = models.DateField(blank=True, null=True)
    status = models.IntegerField()
    state = models.CharField(max_length=16)
    sprint = models.IntegerField()
    slug = models.CharField(max_length=16)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'core_grantgoal'


class DjangoAdminLog(models.Model):
    action_time = models.DateTimeField()
    object_id = models.TextField(blank=True, null=True)
    object_repr = models.CharField(max_length=200)
    action_flag = models.PositiveSmallIntegerField()
    change_message = models.TextField()
    content_type = models.ForeignKey('DjangoContentType', models.DO_NOTHING, blank=True, null=True)
    user = models.ForeignKey(AuthUser, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'django_admin_log'


class DjangoContentType(models.Model):
    app_label = models.CharField(max_length=100)
    model = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'django_content_type'
        unique_together = (('app_label', 'model'),)


class DjangoMigrations(models.Model):
    id = models.BigAutoField(primary_key=True)
    app = models.CharField(max_length=255)
    name = models.CharField(max_length=255)
    applied = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_migrations'


class DjangoSession(models.Model):
    session_key = models.CharField(primary_key=True, max_length=40)
    session_data = models.TextField()
    expire_date = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'django_session'


class HomeEmpleados(models.Model):
    id = models.BigAutoField(primary_key=True)
    noempleado = models.IntegerField(db_column='NoEmpleado')  # Field name made lowercase.
    nombrepila = models.CharField(db_column='NombrePila', max_length=50)  # Field name made lowercase.
    primerapellido = models.CharField(db_column='PrimerApellido', max_length=50)  # Field name made lowercase.
    segundoapellido = models.CharField(db_column='SegundoApellido', max_length=50)  # Field name made lowercase.
    calle = models.CharField(db_column='Calle', max_length=100)  # Field name made lowercase.
    numext = models.CharField(db_column='NumExt', max_length=10)  # Field name made lowercase.
    colonia = models.CharField(db_column='Colonia', max_length=50)  # Field name made lowercase.
    numtel = models.CharField(db_column='NumTel', max_length=15)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'home_empleados'


class HomeUser(models.Model):
    id = models.BigAutoField(primary_key=True)
    password = models.CharField(max_length=128)
    last_login = models.DateTimeField(blank=True, null=True)
    is_superuser = models.IntegerField()
    username = models.CharField(unique=True, max_length=150)
    is_staff = models.IntegerField()
    is_active = models.IntegerField()
    date_joined = models.DateTimeField()
    email = models.CharField(unique=True, max_length=254)
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=30)

    class Meta:
        managed = False
        db_table = 'home_user'


class HomeUserGroups(models.Model):
    id = models.BigAutoField(primary_key=True)
    user = models.ForeignKey(HomeUser, models.DO_NOTHING)
    group = models.ForeignKey(AuthGroup, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'home_user_groups'
        unique_together = (('user', 'group'),)


class HomeUserUserPermissions(models.Model):
    id = models.BigAutoField(primary_key=True)
    user = models.ForeignKey(HomeUser, models.DO_NOTHING)
    permission = models.ForeignKey(AuthPermission, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'home_user_user_permissions'
        unique_together = (('user', 'permission'),)


class HomeUserprofile(models.Model):
    id = models.BigAutoField(primary_key=True)
    timestamp = models.DateField()
    updated = models.DateField()
    bio = models.CharField(max_length=256)
    status = models.IntegerField()
    user = models.OneToOneField(HomeUser, models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'home_userprofile'
